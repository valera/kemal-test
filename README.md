# kemal-test

A port of my [40k counter](https://github.com/Timecrash/40k_counter) from Ruby/Sinatra to Crystal/Kemal.

## Installation

TODO: Write installation instructions here

## Usage

TODO: Write usage instructions here

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://git.disroot.org/timecrash/kemal-test/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [Valera Velez](https://git.disroot.org/timecrash) - creator and maintainer
